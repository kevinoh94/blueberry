package com.example.blueberry.viewmodels;

public class Ringo {

    private int connHandle;
    private int name;
    private String status;
    private String gesture;

    public Ringo(int connHandle, int name) {
        this.connHandle = connHandle;
        this.name = name;
        this.gesture = "Released";
    }

    public int getConnHandle() {
        return connHandle;
    }

    public void setConnHandle(int connHandle) {
        this.connHandle = connHandle;
    }

    public int getName() {
        return name;
    }

    public void setName(int name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getGesture() {
        return gesture;
    }

    public void setGesture(String gesture) {
        this.gesture = gesture;
    }

}
