package com.example.blueberry.views.ringo;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.blueberry.R;
import com.example.blueberry.adapter.DiscoveredBluetoothDevice;

public class RingoViewActivity extends FragmentActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multi_ringo);

        final DiscoveredBluetoothDevice gateway = (DiscoveredBluetoothDevice) getIntent().getParcelableExtra("gateway");
        getIntent().putExtra("gateway", gateway);

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.add(R.id.ringoGridFragment, new RingoGridFragment());
        ft.commit();
    }
}
