package com.example.blueberry.views.ringo;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.example.blueberry.R;
import com.example.blueberry.adapter.DiscoveredBluetoothDevice;
import com.example.blueberry.adapter.RingoGridAdapter;
import com.example.blueberry.viewmodels.Ringo;
import com.example.blueberry.viewmodels.RingoViewModelPattern;

import java.util.ArrayList;
import java.util.List;

public class RingoGridFragment extends Fragment implements LedToggle{
    private RingoViewModelPattern mViewModel;
    private GridView grid;
    private RingoGridAdapter Adapter;
    private List<Ringo> devices;

    LedToggle mLedToggle;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_ringo_grid, container, false);
        grid = view.findViewById(R.id.grid);

        final Intent intent = getActivity().getIntent();
        final DiscoveredBluetoothDevice gateway = intent.getParcelableExtra("gateway");

        mViewModel = ViewModelProviders.of(this).get(RingoViewModelPattern.class);
        mViewModel.connect(gateway);

        devices = mViewModel.getRingos();

        Adapter = new RingoGridAdapter(getContext(), devices, mLedToggle);
        grid.setAdapter(Adapter);

        mViewModel.getButtonState().observe(this,
                pressed -> Adapter.notifyDataSetChanged());

        mViewModel.getDeviceState().observe(this, connected -> Adapter.notifyDataSetChanged());
        return view;
    }

    @Override
    public void toggleLed(Ringo ringo, boolean isOn) {
        mViewModel.toggleLed(ringo, isOn);
    }
}
