package com.example.blueberry.views;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.example.blueberry.R;
import com.example.blueberry.views.ringo.RingoViewActivity;

public class MainActivity extends AppCompatActivity {

    private Button scan, device;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        scan = findViewById(R.id.scan);
        device = findViewById(R.id.devices);

        scan.setOnClickListener(v -> {
            Intent intent = new Intent(getBaseContext(), ScannerActivity.class);
            startActivity(intent);
        });

        device.setVisibility(View.GONE);
        device.setOnClickListener(v -> {
            Intent intent = new Intent(getBaseContext(), RingoViewActivity.class);
            startActivity(intent);
        });
    }
}
