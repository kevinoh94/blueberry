package com.example.blueberry.views.ringo;

import com.example.blueberry.viewmodels.Ringo;

public interface LedToggle {
    void toggleLed(Ringo ringo, boolean isOn);
}
