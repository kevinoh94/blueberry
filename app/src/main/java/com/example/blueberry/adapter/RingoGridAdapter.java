package com.example.blueberry.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Switch;
import android.widget.TextView;

import androidx.lifecycle.ViewModelProviders;

import com.example.blueberry.R;
import com.example.blueberry.viewmodels.Ringo;
import com.example.blueberry.viewmodels.RingoViewModelPattern;
import com.example.blueberry.views.ringo.LedToggle;
import com.example.blueberry.views.ringo.RingoGridFragment;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RingoGridAdapter extends BaseAdapter {
    private Context mContext;
    private List<Ringo> devices;
    LedToggle mLedToggle;
    private TextView status, gesture;
    private Switch ledSwitch;

    public RingoGridAdapter(Context mContext, List<Ringo> devices, LedToggle mLedToggle) {
        this.mContext = mContext;
        this.devices = devices;
        this.mLedToggle = mLedToggle;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Ringo ringo = devices.get(position);

        if (convertView == null) {
            final LayoutInflater layoutInflater = LayoutInflater.from(mContext);
            convertView = layoutInflater.inflate(R.layout.ringo_card, null);
        }
        status = convertView.findViewById(R.id.status);
        gesture = convertView.findViewById(R.id.gesture);
        ledSwitch = convertView.findViewById(R.id.led_switch);

//        status.setText(ringo.getStatus());
        status.setText(ringo.getConnHandle());
        gesture.setText(ringo.getGesture());
        ledSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> mLedToggle.toggleLed(ringo, isChecked));

        return convertView;
    }

    public void notifyThis(){
        this.notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return devices.size();
    }
}
